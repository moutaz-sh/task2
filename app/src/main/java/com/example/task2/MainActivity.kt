package com.example.task2

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import de.hdodenhof.circleimageview.CircleImageView
import java.util.jar.Manifest
import kotlin.math.sin


class MainActivity : AppCompatActivity() {
    companion object {
        private val IMAGE_CHOOSE = 1000;
        private val PERMISSION_CODE = 1001;
    }
    var awesomeValidation = AwesomeValidation(ValidationStyle.BASIC)
    lateinit var camera:CircleImageView
    lateinit var profilePic:CircleImageView
    lateinit var singupBtn:Button
    lateinit var email:EditText
    lateinit var password:EditText
    lateinit var fullName:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    fun init(){
        camera=findViewById(R.id.icon_camera)
        profilePic=findViewById(R.id.icon_profile)
        singupBtn=findViewById(R.id.btn_singup)
        email=findViewById(R.id.email)
        password=findViewById(R.id.textpass)
        fullName=findViewById(R.id.full)
        awesomeValidation.addValidation(this,R.id.email, Patterns.EMAIL_ADDRESS, R.string.email_invalid)
        awesomeValidation.addValidation(this, R.id.full, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.name_invalid)
        awesomeValidation.addValidation(this,R.id.textpass,"^.{7,}\$",R.string.password_error)
        camera.setOnClickListener(View.OnClickListener {
            readPermision()
        })
        singupBtn.setOnClickListener(View.OnClickListener {
            validation()
        })
    }
    fun validation(){
        if (awesomeValidation.validate() && profilePic.drawable.constantState!=ResourcesCompat.getDrawable(resources,R.drawable.icon_profile,null)?.constantState) {
            Toast.makeText(this, R.string.sucess, Toast.LENGTH_LONG).show();
        }else if (profilePic.drawable.constantState== ResourcesCompat.getDrawable(resources,R.drawable.icon_profile,null)?.constantState){
            Toast.makeText(this, R.string.profile_invalid, Toast.LENGTH_LONG).show();
        }
    }

    fun readPermision(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_DENIED){
              val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
              requestPermissions(permissions, PERMISSION_CODE)
          } else{
                chooseImageGallery();
        }
    }else{
            chooseImageGallery();
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    chooseImageGallery()
                }else{
                    Toast.makeText(this,"Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    private fun chooseImageGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            profilePic.setImageURI(data?.data)
        }
    }
}